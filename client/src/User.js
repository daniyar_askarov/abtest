import React from 'react';
import {Histogram} from './Histogram';

export class User extends React.Component{

    constructor(props){
        super(props);
        this.state = {index: props.index, user: props.user};
        this.onClick = this.onClick.bind(this);
    }
    onClick(e){
        this.props.onRemove(this.state.user);
    }
    render(){
        return <tr>
            <td>{this.state.index}</td>
            <td>{new Date(this.state.user.dateRegistration).toLocaleDateString('en-GB')}</td>
            <td>{new Date(this.state.user.dateLastActivity).toLocaleDateString('en-GB')}</td>
            {/*<td><button onClick={this.onClick}>Удалить</button></td>*/}
        </tr>;
    }
}

export class UserForm extends React.Component{

    
    constructor(props){
        super(props);   
        var regIsValid = false;
        var actIsValid = false;
        this.state = { dateRegistration: "", dateLastActivity: "", regValid: regIsValid, actValid: actIsValid};

        this.onSubmit = this.onSubmit.bind(this);
        this.onDateRegistrationChange = this.onDateRegistrationChange.bind(this);
        this.onDateLastActivityChange = this.onDateLastActivityChange.bind(this);
    }

    chechValidation(date) {
        const regexddmmyyyy = /^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/;
        return regexddmmyyyy.test(date);
    }

    onDateRegistrationChange(e) {
        var val = e.target.value;
        var valid = this.chechValidation(val);
        this.setState({ dateRegistration: val, regValid: valid });
    }
    onDateLastActivityChange(e) {
        var val = e.target.value;
        var valid = this.chechValidation(val);
        this.setState({ dateLastActivity: val, actValid: valid });
    }
    onSubmit(e) {
        e.preventDefault();
        e.preventDefault();
        var registration = this.state.dateRegistration;
        var lastActivity = this.state.dateLastActivity;
        var regIsValis = this.state.regValid;
        var actIsValis = this.state.actValid;
        if (!registration || !lastActivity || !regIsValis || !actIsValis) {
            alert("Данные введены неверно! Форматы даты: DD-MM-YYYY!");
            return;
        }
        this.props.onUserSubmit({ dateRegistration: registration, dateLastActivity: lastActivity});
        this.setState({ dateRegistration: "", dateLastActivity: ""});
    }
    render() {
        var regColor = this.state.regValid === true ? "green" : "red";
        var actColor = this.state.actValid === true ? "green" : "red";
        return (

            <form onSubmit={this.onSubmit}>
                <h2>Добавить пользователей</h2>
                <p>
                    <input type="text"
                        placeholder="Дата регистрации"
                        value={this.state.dateRegistration}
                        onChange={this.onDateRegistrationChange}
                        style={{ borderColor: regColor }} />
                </p>
                <p>
                    <input type="text"
                        placeholder="Дата последней активности"
                        value={this.state.dateLastActivity}
                        onChange={this.onDateLastActivityChange}
                        style={{ borderColor: actColor }} />
                </p>
                <input type="submit" value="Сохранить" />
            </form>
        );
    }
}

export class UserList extends React.Component{

    
    constructor(props){
        super(props);
        this.state = { users: [], retention : 0.0 };

        this.onAddUser = this.onAddUser.bind(this);
        this.onRemoveUser = this.onRemoveUser.bind(this);
        this.CalulateRetention = this.CalulateRetention.bind(this);
    }

    loadData() {
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' }
        };
        fetch('http://localhost:3001' + this.props.apiUrl , requestOptions)
            .then(response => response.json())
            .then(data => this.setState({ users: data }));
    }

    componentDidMount() {
        this.loadData();
    }

    onAddUser(user) {
        if (user) {
            var pattern = /(\d{2})[- /.](\d{2})[- /.](\d{4})/;

            const userRequest = { 
                dateRegistration : new Date(user.dateRegistration.replace(pattern,'$3-$2-$1')).toISOString(), 
                dateLastActivity : new Date(user.dateLastActivity.replace(pattern,'$3-$2-$1')).toISOString()
            };
            const jsonData = JSON.stringify(userRequest);
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify( userRequest )
            };
            fetch('http://localhost:3001' + this.props.apiUrl, requestOptions)
                .then(response => response.json())
                .then(data => this.loadData());            
            
        }
    }

    onRemoveUser(user) {
        if (user) {
            
            const requestOptions = {
                method: 'Delete',
                headers: { 'Content-Type': 'application/json' }
            };
            fetch('http://localhost:3001' + this.props.apiUrl + "/" + user.guid, requestOptions)
                .then(response => response.json())
                .then(data => this.loadData());
        }        
    }

    CalulateRetention(e) {
        var startDate = "2020-05-10T00:00:00";

        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' }
        };
        fetch('http://localhost:3001' + this.props.apiUrl + "/calculate?startDate=" + startDate, requestOptions)
            .then(response => response.json())
            .then(data => this.setState({ retention: data }));               
    }
    render(){
        var remove = this.onRemoveUser;
        return <div>
            <UserForm onUserSubmit={this.onAddUser} />
            <br/>
            <Histogram users={this.state.users}/>
            <br/>
            <button onClick={this.CalulateRetention}>Вычислить</button>
            <h4>Rolling Retention 7 day: {this.state.retention.toFixed(2)}%</h4>
            <h2>Список пользователей</h2>
            <table>
                <tbody>
                    <tr>
                        <th>№</th>
                        <th>Дата регистрации</th>
                        <th>Дата последней активности</th>
                        {/*<th>Удаление</th>*/}
                    </tr>
                    {
                        this.state.users.map(function (user, index){
                            return <User key={user.guid} index={index + 1} user={user} onRemove={remove} />
                        })
                    }
                </tbody>
            </table>
        </div>;
    }
}