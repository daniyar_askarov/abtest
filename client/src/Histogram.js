import React from 'react';
import CanvasJSReact from './assets/canvasjs.react';
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export class Histogram extends React.Component {	
    constructor(props){
        super(props);
        this.state = { mappedArray: [] };
    }

	render() {
        this.props.users.forEach((item, index) => {
            var diff = (new Date(item.dateLastActivity) - new Date(item.dateRegistration)) / 86400000;
            this.state.mappedArray[index] = { x: index + 1, y: diff };
        });
        console.log(this.state.mappedArray);

		const options = {
			animationEnabled: true,
			exportEnabled: true,
			theme: "light2", //"light1", "dark1", "dark2"
			title:{
				text: "Users LifeCycle"
			},
			axisY: {
				includeZero: true
			},
			data: [{
				type: "column", //change type to bar, line, area, pie, etc
				//indexLabel: "{y}", //Shows y value on all Data Points
				indexLabelFontColor: "#5A5757",
				indexLabelPlacement: "outside",
				dataPoints: this.state.mappedArray
			}]
		}
		
		return (
		<div>
			<CanvasJSChart options = {options} 
				/* onRef={ref => this.chart = ref} */
			/>
			{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
		</div>
		);
	}
}