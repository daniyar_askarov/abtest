import logo from './logo.svg';
import './App.css';
import {User, UserForm, UserList} from './User';

function App() {
  return (
    <div className="App">
      <UserList apiUrl="/api/users" />
    </div>
  );
}

export default App;
