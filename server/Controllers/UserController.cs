﻿using ABTestHistorgram.DAL;
using ABTestHistorgram.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABTestHistorgram.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UserController : ControllerBase
    {
        private ApplicationContext data;
        public UserController(ApplicationContext context)
        {
            data = context;
        }
        [HttpGet]
        public async Task<IEnumerable<User>> Get()
        {
            return await data.Users.ToListAsync();
        }

        [HttpGet("calculate")]
        public double Calculate()
        {
            var activeUsers = data.Users.Where(u => u.DateLastActivity >= u.DateRegistration.AddDays(7)).Count();
            var allUsers = data.Users.Count();
            return (double)activeUsers/(double)allUsers *100;
        }

        [HttpPost]
        public async Task<IActionResult> Post(User user)
        {
            user.Guid = Guid.NewGuid().ToString();
            data.Users.Add(user);
            await data.SaveChangesAsync();
            return Ok(user);
        }

        [HttpDelete("{guid}")]
        public async Task<IActionResult> Delete(string guid)
        {
            User user = data.Users.FirstOrDefault(x => x.Guid == guid); ;
            if (user == null)
            {
                return NotFound();
            }
            data.Users.Remove(user);
            await data.SaveChangesAsync();
            return Ok(user);
        }
    }
}
