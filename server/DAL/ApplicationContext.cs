﻿using ABTestHistorgram.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABTestHistorgram.DAL
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=usersdb;Username=postgres;Password=password");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasKey(p => new { p.Guid });
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 05, 12)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 05, 15)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 05, 11)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 05, 16)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 05, 14)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 05, 13)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 06, 17)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 06, 04)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 07, 12)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 07, 14)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 07, 14)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 07, 15)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 05, 10)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 05, 16)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 06, 02)
                },
                new User
                {
                    Guid = Guid.NewGuid().ToString(),
                    DateRegistration = new DateTime(2020, 05, 10),
                    DateLastActivity = new DateTime(2020, 06, 19)
                });
        }
    }
}
